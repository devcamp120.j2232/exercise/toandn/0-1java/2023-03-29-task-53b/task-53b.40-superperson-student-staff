import models.Person;
import models.Staff;
import models.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Nguyen Van A", "Sai Gon");
        Person person2 = new Person("Nguyen Van B", "Ha Noi");
        System.out.println("Person 1: " + person1.toString());
        System.out.println("Person 2: " + person2.toString());

        Student student1 = new Student("Lai Thi C", "Tien Giang", "Yoga", 3, 100000);
        Student student2 = new Student("Le Chi D", "Ca Mau", "Erobic", 4, 150000);
        System.out.println("Student 1: " + student1.toString());
        System.out.println("Student 2: " + student2.toString());

        Staff staff1 = new Staff("Thai Thi E", "Thai Binh", "HCMUTE", 500000);
        Staff staff2 = new Staff("Quach Chan F", "Binh Duong", "HCMUT", 520000);
        System.out.println("Staff 1: " + staff1.toString());
        System.out.println("Staff 2: " + staff2.toString());
    }
}
